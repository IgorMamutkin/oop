package by.netcracker.mamutkin.service;

import by.netcracker.mamutkin.entity.Airport;
import by.netcracker.mamutkin.entity.FlyingObject;

import java.util.ArrayList;

public class ExtremeValue {

    private static ArrayList<FlyingObject> airplanes = Airport.getAirplanes();

    public static int isDear() {
        int max = 0;
        for (FlyingObject flag : airplanes) {
            if (flag.getPrice() > max) {
                max = flag.getPrice();
            }
        }
        return max;
    }

    public static int isCheap() {
        int min = isDear();
        for (FlyingObject flag : airplanes) {
            if (flag.getPrice() < min) {
                min = flag.getPrice();
            }
        }
        return min;
    }

    public static ArrayList<FlyingObject> inTheRangePrice(int min, int max) {
        ArrayList<FlyingObject> flagList = new ArrayList<>();
        for (FlyingObject flag : airplanes) {
            if (flag.getPrice() > min && flag.getPrice() < max) {
                flagList.add(flag);
            }
        }
        return flagList;
    }
}
