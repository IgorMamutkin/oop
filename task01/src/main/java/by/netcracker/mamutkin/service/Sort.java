package by.netcracker.mamutkin.service;

import by.netcracker.mamutkin.entity.Airbus;
import by.netcracker.mamutkin.entity.Airport;
import by.netcracker.mamutkin.entity.FlyingObject;

import java.util.ArrayList;

public class Sort {

    private static ArrayList<FlyingObject> airplanes = Airport.getAirplanes();

    public static void priceSort() {
        for (int i = 0; i < airplanes.size() - 1; i++) {
            for (int j = airplanes.size() - 1; j > i; j--) {
                if (airplanes.get(i).getPrice() > airplanes.get(j).getPrice()) {
                    FlyingObject flag = airplanes.get(i);
                    airplanes.set(i, airplanes.get(j));
                    airplanes.set(j, flag);
                }
            }
        }
    }

    public static void capacitySort() {
        for (int i = 0; i < airplanes.size() - 1; i++) {
            for (int j = airplanes.size() - 1; j > i; j--) {
                if (airplanes.get(i).getCapacity() > airplanes.get(j).getCapacity()) {
                    FlyingObject flag = airplanes.get(i);
                    airplanes.set(i, airplanes.get(j));
                    airplanes.set(j, flag);
                }
            }
        }
    }

    public static ArrayList<Airbus> ticketCoastSort() {
        ArrayList<Airbus> airbuses = new ArrayList<>();
        for (FlyingObject flag : airplanes) {
            if (flag instanceof Airbus) {
                airbuses.add((Airbus) flag);
            }
        }
        for (int i = 0; i < airbuses.size() - 1; i++) {
            for (int j = airbuses.size() - 1; j > i; j--) {
                if (airbuses.get(i).getCapacity() > airbuses.get(j).getCapacity()) {
                    Airbus flag = airbuses.get(i);
                    airbuses.set(i, airbuses.get(j));
                    airbuses.set(j, flag);
                }
            }
        }
        return airbuses;
    }
}
