package by.netcracker.mamutkin.service;

import by.netcracker.mamutkin.entity.AirPlane;
import by.netcracker.mamutkin.entity.Airbus;
import by.netcracker.mamutkin.entity.Airport;
import by.netcracker.mamutkin.entity.FlyingObject;

import java.util.ArrayList;

public class TotalAmount {

    private static ArrayList<FlyingObject> airplanes = Airport.getAirplanes();

    public static int allPrice() {
        int count = 0;
        for (FlyingObject flag : airplanes) {
            count += flag.getPrice();
        }
        return count;
    }

    public static int allCapacity() {
        int count = 0;
        for (FlyingObject flag : airplanes) {
            count += flag.getCapacity();
        }
        return count;
    }

    public static double allTrunkVolume() {
        double count = 0;
        for (FlyingObject flag : airplanes) {
            if (flag instanceof AirPlane) {
                count += ((AirPlane) flag).getTrunkVolume();
            }
        }
        return count;
    }

    public static int allTicketCoast() {
        int count = 0;
        for (FlyingObject flag : airplanes) {
            if (flag instanceof Airbus) {
                count += ((Airbus) flag).getTicketCoast();
            }
        }
        return count;
    }

    public static int allNumberOfStaff() {
        int count = 0;
        for (FlyingObject flag : airplanes) {
            if (flag instanceof Airbus) {
                count += ((Airbus) flag).getNumberOfStaff();
            }
        }
        return count;
    }

}
