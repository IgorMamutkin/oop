package by.netcracker.mamutkin.entity;

import java.util.ArrayList;

public class Airport {
    private static ArrayList<FlyingObject> airplanes = new ArrayList<>();

    public static ArrayList<FlyingObject> getAirplanes() {
        return airplanes;
    }

    public static void addAirplanes(ArrayList<FlyingObject> airplanes) {
        Airport.airplanes = airplanes;
    }

    public static void addAirplanes(FlyingObject airplanes) {
        Airport.airplanes.add(airplanes);
    }

}
