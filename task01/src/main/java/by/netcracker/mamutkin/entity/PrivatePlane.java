package by.netcracker.mamutkin.entity;

import java.util.Objects;

public class PrivatePlane extends AirPlane {
    private boolean isRent; // partial or rent

    public PrivatePlane(int price, double tankVolume, double capacity, int rangуOfFlight, String name,
                        double wingspan, double trunkVolume, int maxSpeed, boolean isRent) {
        super(price, tankVolume, capacity, rangуOfFlight, name, wingspan, trunkVolume, maxSpeed);
        this.isRent = isRent;
    }

    public boolean isRent() {
        return isRent;
    }

    public void setRent(boolean rent) {
        isRent = rent;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PrivatePlane)) return false;
        if (!super.equals(o)) return false;
        PrivatePlane that = (PrivatePlane) o;
        return isRent == that.isRent;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), isRent);
    }

    @Override
    public String toString() {
        return "PrivatePlane : " + super.toString() +
                " isRent = " + isRent +
                ";";
    }
}
