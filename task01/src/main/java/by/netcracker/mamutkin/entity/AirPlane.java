package by.netcracker.mamutkin.entity;

import java.util.Objects;

public class AirPlane extends FlyingObject {

    private double wingspan; // in meters
    private double trunkVolume;
    private int maxSpeed; // km/h

    public AirPlane(int price, double tankVolume, double capacity, int rangуOfFlight, String name, double wingspan,
                    double trunkVolume, int maxSpeed) {
        super(price, tankVolume, capacity, rangуOfFlight, name);
        this.wingspan = wingspan;
        this.trunkVolume = trunkVolume;
        this.maxSpeed = maxSpeed;
    }

    public double getWingspan() {
        return wingspan;
    }

    public void setWingspan(double wingspan) {
        this.wingspan = wingspan;
    }

    public double getTrunkVolume() {
        return trunkVolume;
    }

    public void setTrunkVolume(double trunkVolume) {
        this.trunkVolume = trunkVolume;
    }

    public int getMaxSpeed() {
        return maxSpeed;
    }

    public void setMaxSpeed(int maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AirPlane)) return false;
        if (!super.equals(o)) return false;
        AirPlane airPlane = (AirPlane) o;
        return Double.compare(airPlane.wingspan, wingspan) == 0 &&
                Double.compare(airPlane.trunkVolume, trunkVolume) == 0 &&
                maxSpeed == airPlane.maxSpeed;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), wingspan, trunkVolume, maxSpeed);
    }

    @Override
    public String toString() {
        return super.toString() + " wingspan = " + wingspan +
                ", trunkVolume = " + trunkVolume +
                ", maxSpeed = " + maxSpeed;
    }
}
