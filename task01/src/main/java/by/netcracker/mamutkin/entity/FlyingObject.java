package by.netcracker.mamutkin.entity;

import java.util.Objects;

public class FlyingObject {
    private int price; //how much is the plane in million $
    private double tankVolume;
    private double capacity; // how much person can be carried
    private int rangуOfFlight; // flight range without refueling in km
    private String name;

    public FlyingObject(int price, double tankVolume, double capacity, int rangуOfFlight, String name) {
        this.price = price;
        this.tankVolume = tankVolume;
        this.capacity = capacity;
        this.rangуOfFlight = rangуOfFlight;
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public double getTankVolume() {
        return tankVolume;
    }

    public void setTankVolume(double tankVolume) {
        this.tankVolume = tankVolume;
    }

    public double getCapacity() {
        return capacity;
    }

    public void setCapacity(double capacity) {
        this.capacity = capacity;
    }

    public int getRangуOfFlight() {
        return rangуOfFlight;
    }

    public void setRangуOfFlight(int rangуOfFlight) {
        this.rangуOfFlight = rangуOfFlight;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof FlyingObject)) return false;
        FlyingObject that = (FlyingObject) o;
        return price == that.price &&
                Double.compare(that.tankVolume, tankVolume) == 0 &&
                Double.compare(that.capacity, capacity) == 0 &&
                rangуOfFlight == that.rangуOfFlight &&
                Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(price, tankVolume, capacity, rangуOfFlight, name);
    }

    @Override
    public String toString() {
        return "price = " + price +
                ", tankVolume = " + tankVolume +
                ", capacity = " + capacity +
                ", rangуOfFlight = " + rangуOfFlight +
                ", name = " + name;
    }
}
