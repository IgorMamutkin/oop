package by.netcracker.mamutkin.entity;

import java.util.Objects;

public class Airbus extends AirPlane {

    private int ticketCoast; //in $
    private int numberOfStaff;

    public Airbus(int price, double tankVolume, double capacity, int rangуOfFlight, String name, double wingspan,
                  double trunkVolume, int maxSpeed, int ticketCoast, int numberOfStaff) {
        super(price, tankVolume, capacity, rangуOfFlight, name, wingspan, trunkVolume, maxSpeed);
        this.ticketCoast = ticketCoast;
        this.numberOfStaff = numberOfStaff;
    }

    public int getTicketCoast() {
        return ticketCoast;
    }

    public void setTicketCoast(int ticketCoast) {
        this.ticketCoast = ticketCoast;
    }

    public int getNumberOfStaff() {
        return numberOfStaff;
    }

    public void setNumberOfStaff(int numberOfStaff) {
        this.numberOfStaff = numberOfStaff;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Airbus)) return false;
        if (!super.equals(o)) return false;
        Airbus airbus = (Airbus) o;
        return ticketCoast == airbus.ticketCoast &&
                numberOfStaff == airbus.numberOfStaff;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), ticketCoast, numberOfStaff);
    }

    @Override
    public String toString() {
        return "Airbus : " + super.toString() +
                " ticketCoast = " + ticketCoast +
                ", numberOfStaff = " + numberOfStaff +
                ";";
    }
}
