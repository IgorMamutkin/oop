package by.netcracker.mamutkin.entity;

import java.util.Objects;

public class Fighter extends AirPlane {

    private boolean isMilitary;

    public Fighter(int price, double tankVolume, double capacity, int rangуOfFlight, String name, double wingspan,
                   double trunkVolume, int maxSpeed, boolean isMilitary) {
        super(price, tankVolume, capacity, rangуOfFlight, name, wingspan, trunkVolume, maxSpeed);
        this.isMilitary = isMilitary;
    }

    public boolean isMilitary() {
        return isMilitary;
    }

    public void setMilitary(boolean military) {
        isMilitary = military;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Fighter)) return false;
        if (!super.equals(o)) return false;
        Fighter fighter = (Fighter) o;
        return isMilitary == fighter.isMilitary;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), isMilitary);
    }

    @Override
    public String toString() {
        return "Fighter : " + super.toString() +
                " isMilitary = " + isMilitary +
                ";";
    }
}
