package by.netcracker.mamutkin.controller;

import by.netcracker.mamutkin.entity.*;
import by.netcracker.mamutkin.service.ExtremeValue;
import by.netcracker.mamutkin.service.Sort;

public class Main {
    public static void main(String[] args) {

        Airport.addAirplanes(new Airbus(5, 1000.0, 1000.0, 2000, "Cracker",
                50, 1000.0, 800, 120, 6));
        Airport.addAirplanes(new Airbus(7, 1200.0, 1100.0, 2020, "Cracker2",
                55, 900.0, 900, 180, 5));
        Airport.addAirplanes(new Airbus(4, 800.0, 1100.0, 1200, "Cracker3",
                40, 800.0, 900, 125, 5));
        Airport.addAirplanes(new Fighter(8, 400.0, 100.0, 1500, "Cracker4",
                20, 0.0, 1200, true));
        Airport.addAirplanes(new PrivatePlane(6, 800.0, 1200.0, 1600, "Cracker5",
                350, 800.0, 1000, false));

        Sort.priceSort();
        for (FlyingObject flag: Airport.getAirplanes()) {
            System.out.println(flag);
        }
        System.out.println();
        Sort.capacitySort();
        for (FlyingObject flag: Airport.getAirplanes()) {
            System.out.println(flag);
        }
        System.out.println();
        for (Airbus flag:Sort.ticketCoastSort()) {
            System.out.println(flag);
        }
        System.out.println();
        for (FlyingObject flag: ExtremeValue.inTheRangePrice(1,5)) {
            System.out.println(flag);
        }

    }
}
